# Abstract

La 1ère fois que j'ai suivi un tutorial wasm, j'étais content d'avoir "buildé" un projet Rust pour faire une addition et de l'utiliser dans mon navigateur, mais très rapidement déçu de ne même pas pouvoir passer une string en paramètre à mes fonctions ... Bref ça me semblait "pas cuit".
Aujourd'hui, Wasm est encore jeune, mais a bien grandi, on peut l'utiliser dans son navigateur mais aussi côté serveur. De plus en plus de langages "compilent du wasm", notamment Go. Après quelques mois d'expérimentations, j'ai pas mal avancé sur le sujet et j'ai de quoi alimenter un atelier avec le contenu suivant:

- Petite introduction à Wasm
- Wasm dans le navigateur avec Go (et TinyGo)
- Modification du DOM
- Passage de paramètres (Strings, JSon, Array)
- Wasm avec NodeJS
- avec Go & TinyGo
- un peu de Rust (pas d'inquiétude, le code sera simple)
- Wasm avec Wasmer, Wagi et Grain (un nouveau langage fonctionnel qui compile uniquement en Wasm)
- Et pour conclure, si on faisait notre propre "FaaS" ? (un POC, nous n'avons que 3 heures 😉)
- Avec TinyGo et Fastify (framework NodeJS, un peu le successeur d'ExpressJS)
- Avec Rust et Fastify

🖐️ Pas besoin d'être un champion de Go ou Rust (moi-même je ne le suis pas)
👋 C'est bien d'avoir des notions de JavaScript tout de même (pas besoin de CSS 😉)

Au niveau software, vous n'avez rien à installer, tout se fait avec l'IDE Cloud GitPod (https://www.gitpod.io/)
1️⃣ C'est la 1ère "exigence" pour participer à l'atelier
ℹ️ le plan gratuit de GitPod est largement suffisant pour faire l'atelier

2️⃣La 2ème exigence: l'atelier est sur GitLab 🦊 donc il vous faut un compte GitLab (néanmoins si ça vous stresse, vous devriez pouvoir cloner le projet sur GitHub et l'utiliser avec GitPod)


